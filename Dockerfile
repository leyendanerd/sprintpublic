FROM maven:3.6.3-jdk-11
WORKDIR /usr/src/app
ENV PORT 8080
EXPOSE $PORT
COPY  ./target/JAR_PATH run-demo-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","run-demo-0.0.1-SNAPSHOT.jar"]
