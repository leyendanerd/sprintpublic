#!/bin/bash
#==========================================
#GENERADOR DE PROYECTOS GITLAB
#Prodigio.tech
#Autor: Samuel Garcia
#==========================================
#VARIABLES
#TOKENGITLAB="";
#NAMESPACEID=58050510;
#IDROOTPROJECT=39801779;

RULEXMLPATH="rule.xml"
RULEXSDPATH="rule.xsd"

ERROR=""
QUEUEREPOEXISTENTE=""
QUEUEREPOERROR=""
QUEUEREPOCREADOS=""

CONTADOR_REPOS_EXISTENTES=0
CONTADOR_REPOS_ERROR=0
CONTADOR_REPOS_CREADOS=0

echo INFO VARS INPUT
#echo NAMESPACEID=$NAMESPACEID;
echo RULEXMLPATH=$RULEXMLPATH
echo RULEXSDPATH=$RULEXSDPATH
echo IDROOTPROJECT=$IDROOTPROJECT

echo GROUPNAME=$GROUPNAME
echo PROJECTNAME=$PROJECTNAME
echo PROJECTNAME_DESC=$PROJECTNAME_DESC
echo IMPORTURL=$IMPORTURL
echo ISSUEID=$ISSUEID
echo REQNAME=$REQNAME

#VALIDAR EXISTENCIA DE PARSER XML
if [ $(ls -l $RULEXMLPATH | wc -l) -lt 1 ]; then
    ERROR="Error  (003) - $RULEXMLPATH fichero no encontrado"
    echo $ERROR
    exit 1
fi

if [ $(ls -l $RULEXSDPATH | wc -l) -lt 1 ]; then
    ERROR="Error  (004) - $RULEXSDPATH fichero no encontrado"
    echo $ERROR
    exit 1
fi




## Obtener el ID del proyecto heredado
#MMM="/bchi/sprintboot-demo.git"
MMM=`echo $IMPORTURL | sed -e 's/^\///' -e 's/\//%2F/g' -e 's/.git//'`
echo "Buscando ID para el repo :$MMM"
ID_PROJECTIMPORT=$(curl -s --header "PRIVATE-TOKEN: ${TOKENGITLAB}" "https://gitlab.com/api/v4/projects/${MMM}")
ID_PROJECTIMPORT=`echo $ID_PROJECTIMPORT | jq '.id'`
echo "ID ENCONTRADO :$ID_PROJECTIMPORT"



#VERIFICAR LA EXISTENCIA DE CADA REPO; Y SI NO EXISTE CREAR

#OBTENER EL ID DEL NAMESPACE
#NAMESPACEID=$(curl --header "PRIVATE-TOKEN: ${TOKENGITLAB}" "https://gitlab.com/api/v4/namespaces?search=$GROUPNAME" | awk -F ":" '/id/{print $2}' | awk -F "," '{print $1}')
NAMESPACEID=$(curl --header "PRIVATE-TOKEN: ${TOKENGITLAB}" "https://gitlab.com/api/v4/groups?search=$GROUPNAME&order_by=id" | awk -F ":" '/id/{print $2}' | awk -F "," '{print $1}')
echo "NAMESPACEID Encontrado =$NAMESPACEID, desde $GROUPNAME"
if [ -z "$NAMESPACEID" ]; then
    echo "Error  (004.1) - El grupo  $GROUPNAME no ha sido encontrado, favor verificar: $NAMESPACEID"
    exit 1
fi

RESPONSE=$(curl --header "PRIVATE-TOKEN: ${TOKENGITLAB}" "https://gitlab.com/api/v4/groups/${NAMESPACEID}/projects?search=${PROJECTNAME}")
echo RESPONSE=$RESPONSE

EXISTE=$(echo $RESPONSE | wc -c)
echo EXISTE=$EXISTE

if [ $EXISTE -lt 4 ]; then
    #CREAR EL REPO
    #EXTRAER TODOS LOS VALORES REQUERIDOS
    #ESTO AHORA VIENE DESDE LAS VARS PIPELINE

    #VERIFICACION DE NOMENCLATURA PERMITIDA
    REGX=$(xmllint --xpath "/rule/regular-expression/text()" $RULEXMLPATH)
    CUMPLE=$([[ $PROJECTNAME =~ $REGX ]] && echo "yes" || echo "no")

    if [ "$CUMPLE" = "yes" ]; then
        echo "SI CUMPLE CON LA TAXONOMIA"

        #VERIFICACION DE TIPO DE CREACION (1=CON MOLDE, 2=CON TEMPLATE GITLAB, 3=SIMPLE)
        if [[ $TIPO -lt 1 || $TIPO -gt 3 ]]; then
            echo "$PROJECTNAME"="TIPO DE REPO NO SORPORTADO"
            QUEUEREPOERROR=$QUEUEREPOERROR""$PROJECTNAME"=TIPO DE REPO NO SORPORTADO|"
        else
            echo "TIPO DE REPO VALIDADO OK"

            #INSERCION REPO CON PLANTILLA CUSTOM
            if [ $TIPO -eq 1 ]; then
                echo "TIPO DE INSERCION=1"
                echo NAMESPACEID=$NAMESPACEID

                PREFIJO="https://username:"$TOKENGITLAB"@gitlab.com"
                PRIVATEIMPORTURL=$PREFIJO$IMPORTURL

                curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" --header "Content-Type: application/json" --data "{\"name\": \"$PROJECTNAME\", \"description\": \"$PROJECTNAME_DESC\", \"path\": \"$PROJECTNAME\", \"namespace_id\": \"$NAMESPACEID\", \"initialize_with_readme\": \"false\", \"import_url\": \"$PRIVATEIMPORTURL\"}" --url 'https://gitlab.com/api/v4/projects/' -D reponseheaders.txt >reponsebody.txt

                http_code=$(cat reponseheaders.txt | awk '/HTTP/ {print $2}')
                

                if [ $http_code -eq 201 ]; then
                    CONTADOR_REPOS_CREADOS=$(($CONTADOR_REPOS_CREADOS + 1))
                    QUEUEREPOCREADOS="${QUEUEREPOCREADOS} TIPO=${TIPO}, REPO=${PROJECTNAME}:OK|"
                    IDPROYECTO_NEW=`cat reponsebody.txt | jq '.id'`
                    echo "ID NUEVO PROYECTO: $IDPROYECTO_NEW"

                    # deshabilitar la opcion de eliminar el branch al hacer merge
                    printf "\n\n# Deshabilitar eliminación auto de branch\n"
                    curl -s --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}" --data "remove_source_branch_after_merge=false"

                    # copiar branch protegidos
                    printf "\n\n# copiar branch protegidos\n"
                    PROT_BRANCH=$(curl -s --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${ID_PROJECTIMPORT}/protected_branches")
                    for row in $(echo "$PROT_BRANCH" | jq -r '.[] | @base64'); do
                       _jq() { (echo ${row} | base64 --decode | jq -c .  )}
                       P_B="$(_jq)"  
                       # cambiar id de retorno por id de proyecto, segun doc
                       P_B=`echo $P_B | sed -e "s/\"id\":[1-9]*,/\"id\":${IDPROYECTO_NEW},/g"`
                       echo $P_B > tmp.json
                       curl -s  --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/protected_branches" --header 'Content-Type: application/json' --data @tmp.json 
                    done

                    ## copiar las reglas de aprobacion ( depende de los branch protegidos)
                    printf "\n\n# copiar las reglas de aprobacion\n"
                    RULES=$(curl -s  --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${ID_PROJECTIMPORT}/approval_rules")
                    for row in $(echo "$RULES" | jq -r '.[] | @base64'); do
                       _jq() { (echo ${row} | base64 --decode | jq -cr "${1}"   )}
                     
                       A_NAME=$(_jq '.name')
                       A_A_R=$(_jq '.approvals_required')
                       A_A_T_A_PB=$(_jq '.applies_to_all_protected_branches')
                       A_RR_T=$(_jq '.rule_type')
                       A_GROUPS_ID=$(_jq ' [ .groups | .[] | .id | tostring ]')  
                       A_USERS_ID=$(_jq ' [ .users | .[] | .id | tostring ]')  
                       A_USERS_UN=$(_jq ' [ .users | .[] | .username | tostring ]')  
                       P_T_BR_NAME=$(_jq '.protected_branches | first | .name')

                       PROT_BRANCH_ID=$(curl -s --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/protected_branches/${P_T_BR_NAME}" | jq -cr '[.id  | tostring ]')
                       echo "{\"id\": \"${IDPROYECTO_NEW}\", \"name\":  \"${A_NAME}\", \"approvals_required\": \"${A_A_R}\", \"applies_to_all_protected_branches\": \"${A_A_T_A_PB}\", \"group_ids\": ${A_GROUPS_ID}, \"protected_branch_ids\": ${PROT_BRANCH_ID} ,\"rule_type\": \"${A_RR_T}\",\"user_ids\": ${A_USERS_ID},\"usernames\": ${A_USERS_UN}}" > tmp.json
                       curl -s --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/approval_rules" --header 'Content-Type: application/json' --data @tmp.json 
                    done

                    ## copiar los flag de reglas de aprobacion
                    
                    printf "\n\n# copiar aprovals\n"
                    APPROVALS=$(curl -s --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${ID_PROJECTIMPORT}/approvals")
                    echo "$APPROVALS" > tmp.json
                    curl -s --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/approvals" --header 'Content-Type: application/json' --data @tmp.json 
               

                    ## copiar variables
                    printf "\n\n# copiar variables\n"
                    VARIABLES=$(curl -s --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${ID_PROJECTIMPORT}/variables")
                    for row in $(echo "$VARIABLES" | jq -r '.[] | @base64'); do
                       _jq() { (echo ${row} | base64 --decode | jq -c .  )}
                       echo "$(_jq)" > tmp.json
                       curl -s --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/variables" --header 'Content-Type: application/json' --data @tmp.json 
                    done

                    ## copiar los environments
                    printf "\n\n# copiar environments\n"
                    DEPLOYMENTS=$(curl -s --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${ID_PROJECTIMPORT}/environments")
                    for row in $(echo "$DEPLOYMENTS" | jq -r '.[] | @base64'); do
                       _jq() { (echo ${row} | base64 --decode | jq -cr  "${1}" )}
                       NAME_ENV=$(_jq '.name')
                       TIER_ENV=$(_jq '.tier')
                       curl -s --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB"  "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/environments" --header "Content-Type: application/json" --data "{\"id\": \"${IDPROYECTO_NEW}\", \"name\": \"${NAME_ENV}\", \"tier\": \"${TIER_ENV}\"}" 
                    done

                    ## copiar environments protegidos
                    printf "\n\n# copiar environments protegidos\n"
                    PROT_ENVIROMENTS=$(curl -s --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/${ID_PROJECTIMPORT}/protected_environments")
                    for row in $(echo "$PROT_ENVIROMENTS" | jq -r '.[] | @base64'); do
                       _jq() { (echo ${row} | base64 --decode | jq -cr  "${1}"  )}
                       NAME_ENV=$(_jq '.name')
                       D_A_L=$(_jq '.deploy_access_levels | del(..| nulls)')
                       # D_A_L=`echo $D_A_L | sed -e 's/"user_id":null,//' -e 's/"group_id":null,//' -e 's/"access_level":null,//'`
                       R_A_C=$(_jq '.required_approval_count')
                       A_R=$(_jq '.approval_rules | del(..| nulls)')
                      echo "{\"id\": \"${IDPROYECTO_NEW}\", \"name\": \"${NAME_ENV}\", \"required_approval_count\": \"${R_A_C}\", \"deploy_access_levels\": ${D_A_L}, \"approval_rules\": ${A_R}}" > tmp.json
                      #echo " ###################"
                      #cat tmp.json
                      #echo "######################"
                      curl -s --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB"  "https://gitlab.com/api/v4/projects/${IDPROYECTO_NEW}/protected_environments" --header "Content-Type: application/json" --data @tmp.json
                    done



                    ##ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER GITLAB)
                    MESSAGE="$REQNAME - The Project requested as ${PROJECTNAME} was created OK"
                    #curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID/notes?body=$MESSAGE" -D reponseheadersnota.txt > reponsebodynota.txt;

                    ## ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER JIRA CLOUD)
                    curl --request POST \
                    --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/comment" \
                    --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
                    --header 'Accept: application/json' \
                    --header 'Content-Type: application/json' \
                    --data "{
  \"body\": {
    \"type\": \"doc\",
    \"version\": 1,
    \"content\": [
      {
        \"type\": \"paragraph\",
        \"content\": [
          {
            \"text\": \"$MESSAGE\",
            \"type\": \"text\"
          }
        ]
      }
    ]
  }
}" -D reponseheadersnota.txt >reponsebodynota.txt

                    ##CERRANDO EL ISSUE (ISSUE TRACKER GITLAB)
                    #curl --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID?state_event=close" -D reponseheadersclose.txt > reponsebodyclose.txt;

                    ##CERRANDO EL ISSUE (ISSUE TRACKER JIRA CLOUD)
                    curl --request POST \
                    --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/transitions" \
                    --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
                    --header 'Accept: application/json' \
                    --header 'Content-Type: application/json' \
                    --data '{
  "transition": {
    "id": "31",
    "looped": true
  },
    "extraData": {
      "Iteration": "10a",
      "Step": "4"
    },
    "description": "From the order testing process",
    "generator": {
      "id": "mysystem-1",
      "type": "mysystem-application"
    },
    "cause": {
      "id": "myevent",
      "type": "mysystem-event"
    },
    "activityDescription": "Complete order processing",
    "type": "myplugin:type"
  }

  }
}' -D reponseheadersclose.txt >reponsebodyclose.txt

                else

                    CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1))
                    ERRORDETAIL=$(cat reponsebody.txt | sed 's/"//g')
                    export QUEUEREPOERROR="${QUEUEREPOERROR} TIPO=${TIPO}, REPO=${PROJECTNAME}: ERROR FROM GITLAB [HTTPCODE=${http_code}, MSG=${ERRORDETAIL}]|"

                fi

            fi

            #INSERCION REPO CON PLANTILLA GITLAB
            if [ $TIPO -eq 2 ]; then

                echo "TIPO DE INSERCION=2"
                curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" --header "Content-Type: application/json" --data "{\"name\": \"$PROJECTNAME\", \"description\": \"$PROJECTNAME_DESC\", \"path\": \"$PROJECTNAME\",\"namespace_id\": \"$NAMESPACEID\", \"initialize_with_readme\": \"false\", \"template_name\": \"$TEMPLATE\"}" --url 'https://gitlab.com/api/v4/projects/' -D reponseheaders.txt >reponsebody.txt

                http_code=$(cat reponseheaders.txt | awk '/HTTP/ {print $2}')

                if [ $http_code -eq 201 ]; then
                    CONTADOR_REPOS_CREADOS=$(($CONTADOR_REPOS_CREADOS + 1))
                    QUEUEREPOCREADOS="${QUEUEREPOCREADOS} TIPO=${TIPO}, REPO=${PROJECTNAME}:OK|"

                    ##ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER GITLAB)
                    MESSAGE="$REQNAME - The Project requested as ${PROJECTNAME} was created OK"
                    #curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID/notes?body=$MESSAGE" -D reponseheadersnota.txt > reponsebodynota.txt;

                    ## ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER JIRA CLOUD)
                    curl --request POST \
                    --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/comment" \
                    --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
                    --header 'Accept: application/json' \
                    --header 'Content-Type: application/json' \
                    --data "{
  \"body\": {
    \"type\": \"doc\",
    \"version\": 1,
    \"content\": [
      {
        \"type\": \"paragraph\",
        \"content\": [
          {
            \"text\": \"$MESSAGE\",
            \"type\": \"text\"
          }
        ]
      }
    ]
  }
}" -D reponseheadersnota.txt >reponsebodynota.txt

                    ##CERRANDO EL ISSUE (ISSUE TRACKER GITLAB)
                    #curl --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID?state_event=close" -D reponseheadersclose.txt > reponsebodyclose.txt;

                    ##CERRANDO EL ISSUE (ISSUE TRACKER JIRA CLOUD)
                    curl --request POST \
                    --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/transitions" \
                    --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
                    --header 'Accept: application/json' \
                    --header 'Content-Type: application/json' \
                    --data '{
  "transition": {
    "id": "31",
    "looped": true
  },
    "extraData": {
      "Iteration": "10a",
      "Step": "4"
    },
    "description": "From the order testing process",
    "generator": {
      "id": "mysystem-1",
      "type": "mysystem-application"
    },
    "cause": {
      "id": "myevent",
      "type": "mysystem-event"
    },
    "activityDescription": "Complete order processing",
    "type": "myplugin:type"
  }

  }
}' -D reponseheadersclose.txt >reponsebodyclose.txt
                else

                    CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1))
                    ERRORDETAIL=$(cat reponsebody.txt | sed 's/"//g')
                    export QUEUEREPOERROR="${QUEUEREPOERROR} TIPO=${TIPO}, REPO=${PROJECTNAME}: ERROR FROM GITLAB [HTTPCODE=${http_code}, MSG=${ERRORDETAIL}]|"

                fi

            fi

            #INSERCION SIMPLE
            if [ $TIPO -eq 3 ]; then
                echo "TIPO DE INSERCION=3"
                curl --write-out "%{http_code}\n" --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" --header "Content-Type: application/json" --data "{\"name\": \"$PROJECTNAME\", \"description\": \"$PROJECTNAME_DESC\", \"path\": \"$PROJECTNAME\",\"namespace_id\": \"$NAMESPACEID\", \"initialize_with_readme\": \"true\"}" --url 'https://gitlab.com/api/v4/projects/' -D reponseheaders.txt >reponsebody.txt

                http_code=$(cat reponseheaders.txt | awk '/HTTP/ {print $2}')
                if [ $http_code -eq 201 ]; then
                    CONTADOR_REPOS_CREADOS=$(($CONTADOR_REPOS_CREADOS + 1))
                    QUEUEREPOCREADOS="${QUEUEREPOCREADOS} TIPO=${TIPO}, REPO=${PROJECTNAME}:OK|"

                    ##ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER GITLAB)
                    MESSAGE="$REQNAME - The Project requested as ${PROJECTNAME} was created OK"
                    #curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID/notes?body=$MESSAGE" -D reponseheadersnota.txt > reponsebodynota.txt;

                    ## ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER JIRA CLOUD)
                    curl --request POST \
                    --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/comment" \
                    --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
                    --header 'Accept: application/json' \
                    --header 'Content-Type: application/json' \
                    --data "{
  \"body\": {
    \"type\": \"doc\",
    \"version\": 1,
    \"content\": [
      {
        \"type\": \"paragraph\",
        \"content\": [
          {
            \"text\": \"$MESSAGE\",
            \"type\": \"text\"
          }
        ]
      }
    ]
  }
}" -D reponseheadersnota.txt >reponsebodynota.txt

                    ##CERRANDO EL ISSUE (ISSUE TRACKER GITLAB)
                    #curl --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID?state_event=close" -D reponseheadersclose.txt > reponsebodyclose.txt;

                    ##CERRANDO EL ISSUE (ISSUE TRACKER JIRA CLOUD)
                    curl --request POST \
                    --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/transitions" \
                    --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
                    --header 'Accept: application/json' \
                    --header 'Content-Type: application/json' \
                    --data '{
  "transition": {
    "id": "31",
    "looped": true
  },
    "extraData": {
      "Iteration": "10a",
      "Step": "4"
    },
    "description": "From the order testing process",
    "generator": {
      "id": "mysystem-1",
      "type": "mysystem-application"
    },
    "cause": {
      "id": "myevent",
      "type": "mysystem-event"
    },
    "activityDescription": "Complete order processing",
    "type": "myplugin:type"
  }

  }
}' -D reponseheadersclose.txt >reponsebodyclose.txt
                else

                    CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1))
                    ERRORDETAIL=$(cat reponsebody.txt | sed 's/"//g')
                    export QUEUEREPOERROR="${QUEUEREPOERROR} TIPO=${TIPO}, REPO=${PROJECTNAME}: ERROR FROM GITLAB [HTTPCODE=${http_code}, MSG=${ERRORDETAIL}]|"

                fi

            fi

        fi
    else
        echo "NO CUMPLE CON LA TAXONOMIA"
        CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1))
        export QUEUEREPOERROR="${QUEUEREPOERROR} REPO=${PROJECTNAME}:NO CUMPLE LA TAXONOMIA|"
    fi

    echo ""
    echo ""

else

    #INFO REPOSITORIO EXISTE
    CONTADOR_REPOS_EXISTENTES=$(($CONTADOR_REPOS_EXISTENTES + 1))
    QUEUEREPOEXISTENTE="${QUEUEREPOEXISTENTE} REPO=${PROJECTNAME}|"
fi

echo "-REPOSITORIOS PRE-EXISTENTES:" $CONTADOR_REPOS_EXISTENTES
echo $QUEUEREPOEXISTENTE
echo
echo "-REPOSITORIOS CON ERRORES:" $CONTADOR_REPOS_ERROR
echo $QUEUEREPOERROR
echo
echo "-REPOSITORIOS INSERTADOS OK:" $CONTADOR_REPOS_CREADOS
echo $QUEUEREPOCREADOS
