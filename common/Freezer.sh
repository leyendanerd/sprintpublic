#!/bin/bash
#==========================================
#Frezeer GITLAB
#Prodigio.tech
#Autor: Samuel Garcia
#mail: samuel.garcia@prodigio.tech
#==========================================

#variables
ID_PROJECT_CURRENT=$CI_PROJECT_ID;
GITLAB_TOKEN=glpat-BzaEebd3zkAieAQz657U

#verificar si esto viene desde un merged
if [ $(echo $CI_COMMIT_DESCRIPTION | grep "See merge request" | wc -l) -gt 0 ]
then 
	

	#verificar cual rama distinta a (master, release y develop) esta en estado de merged pero no en estado protegido
	for ramas in $(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$ID_PROJECT_CURRENT/repository/branches | jq '.[].name')
do
if [ $(echo $ramas | grep "develop" | wc -l) == 0 ] && [ $(echo $ramas | grep "release" | wc -l) == 0 ] &&  [ $(echo $ramas | grep "master" | wc -l) == 0 ] &&  [ $(echo $ramas | grep "main" | wc -l) == 0 ]
 then
	#limpiando la variable de comillas dobles
	ramas=$(echo $ramas |sed 's/"//g');

	#verificar si esta en forma de "merged"
	isMerged=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$ID_PROJECT_CURRENT/repository/branches/$ramas | jq '.merged');

	#verificar si esta en forma de "protected"
	isProteted=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$ID_PROJECT_CURRENT/repository/branches/$ramas | jq '.protected');

	#confirmar si proteje la rama o no
        if [ $(echo $isMerged | grep "true" | wc -l ) -gt 0 ] && [ $(echo $isProteted | grep "false" | wc -l ) -gt 0 ]
	then
		#incluir un commit en blanco releacionado al branch to freeze
PAYLOAD=$(cat << 'JSON'
{
  "branch": "ramas",
  "commit_message": "Freezed Branch",
  "actions": [
   {
      "action": "create",
      "file_path": "snow/ramas-freezed.md",
      "content": "Freezed Branch"
    }
  ]
}
JSON
);

PAYLOAD=$(echo $PAYLOAD | sed 's/ramas/'$ramas'/g');

echo REQ=$PAYLOAD; 

curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" --data "$PAYLOAD" "https://gitlab.com/api/v4/projects/$ID_PROJECT_CURRENT/repository/commits"


		echo "";
		echo "Protejer la rama: " $ramas;
		curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$ID_PROJECT_CURRENT/protected_branches?name=${ramas}&push_access_level=40&merge_access_level=40&unprotect_access_level=40";
	else
		echo "No: "  $ramas ", ismerged: " $isMerged ", isProteted: " $isProteted;

	fi;
 fi;
done;
	
else echo NO SE REUNEN LAS CONDICIONES PARA FREEZE BRANCH;
fi;
